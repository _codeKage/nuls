<?php
/**
 * Created by PhpStorm.
 * User: Joyce Balinquit
 * Date: 28/05/2018
 * Time: 6:01 PM
 */
require "connection.php";
if(isset($_POST["supplier_id"])){
    if(mysqli_num_rows($conn->query("SELECT * FROM `acquisition` WHERE `supplier_id` = '".$_POST["supplier_id"]."' AND `date_deleted` IS NULL")) > 0){
        echo "<script>alert('Cannot Delete Supplier, assigned to one or more Acquisition');window.history.back();</script>";
    } else {
        $stmt = $conn->query("UPDATE `suppliers` SET date_deleted = CURRENT_DATE WHERE supplier_id = '" . $_POST['supplier_id'] . "'");
        if ($stmt) {
            echo "<script>alert('Supplier Deleted Successfully');location.href='Supplier.php';</script>";
        }
    }

}
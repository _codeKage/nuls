<?php
/**
 * Created by PhpStorm.
 * User: Joyce Balinquit
 * Date: 29/05/2018
 * Time: 2:25 PM
 */

require "connection.php";
if(isset($_POST["material_type_id"])){
    if(mysqli_num_rows($conn->query("SELECT * FROM `catalog` WHERE `material_type_id` = '".$_POST["material_type_id"]."' AND `date_deleted` IS NULL")) > 0) {
        echo "<script>alert('Cannot Delete Material Type, Assigned to one or more catalog');window.history.back();</script>";
    }
    else {

        $stmt = $conn->query("UPDATE `material_types` SET date_deleted = CURRENT_DATE WHERE material_type_id = '" . $_POST['material_type_id'] . "'");
        if ($stmt) {
            echo "<script>alert('Material Type Deleted Successfully');location.href='MaterialType.php';</script>";
        }
    }

}
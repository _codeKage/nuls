<?php
include "startup.php";
?>
<!DOCTYPE html>
<html>
<head>
    <!--background-color: #E6BF36;-->

    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="MaterializeCSS/materialize/css/materialize.min.css"  media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="CSS/Style1.css">
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <title>NULRC</title>
</head>
<body>

<div id="container">
    <nav class="nav-background">
        <?php
        include "nav.php";
        ?>
        <a href="#" data-activates="slide-out" class="button-collapse hide-on-large-only"><i class="material-icons">menu</i></a>
        <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li class=""><a href="Acquisition_Report.php">Acquisition Report</a></li>
            <li class=""><a href="Catalog_Report.php">Catalog Report</a></li>
            <li class="active"><a href="Circulation_Report.php">Circulation Reports</a></li>
            <li class=""><a href="Inventory.php">Inventory</a></li>
        </ul>

    </nav>

    <div id="content">

        <div class="row">
            <h6>Date:</h6>
            <div class="input-field col s3">
                <input type="text" class="datepicker" id="to">
                <label class="active center" for="to">To</label>
            </div>
            <div class="input-field col s3">
                <input type="text" class="datepicker" id="from">
                <label class="active center" for="from">From</label>
            </div>
            <div class="col s3">
                <label>Circulation</label>
                <select class="browser-default">
                    <option value="0">Check In</option>
                    <option value="1">Check Out</option>
                </select>
            </div>
            <div class="col s3">
                <a class="waves-effect waves-light btn active" style="margin-top: 11%" href="Circulation_Print.html">Print</a>
            </div>
        </div>

        <table class="responsive-table grey lighten-2">
            <!--<thead>
            <tr>
                <th>Copyright</th>
                <th>Edition</th>
                <th>Format</th>
                <th>Content type term</th>
                <th>Media type term</th>
                <th>Carrier type term</th>
                <th>ISBN</th>
            </tr>
            </thead>-->

            <tbody>
            <tr>
                <td>
                    <b>Culinary Essentials The American Culinary Federation</b> (Copy: NULIB000002024) <br>
                    <i>Checked out 2/27/2018</i> to Sunga, Rhon Christian Del Rosario (BSHRM: 2015-100519)<br>
                    <i>Library Copies still checked out: 0</i>
                </td>
                <td>
                    <b>Due</b> 3/2/2018
                </td>
                <td>
                    <b>TX 928 .M37 2006</b>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Advanced Practical Cookery: A textbook for educational & industry</b> (Copy: NULIB000000242) <br>
                    <i>Checked out 2/27/2018</i> to Ginez, Jim Roger R. (BSHRM: 2017-100464)<br>
                    <i>Library Copies still checked out: 0</i>
                </td>
                <td>
                    <b>Due</b> 3/2/2018
                </td>
                <td>
                    <b>TX 820 .C85 2008</b>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Culinaria Hungary</b> (Copy: NULIB000000256) <br>
                    <i>Checked out 2/27/2018</i> to Wanawan, Renzo B. (BSHRM: 2015-2017-100152)<br>
                    <i>Library Copies still checked out: 0</i>
                </td>
                <td>
                    <b>Due</b> 3/2/2018
                </td>
                <td>
                    <b>TX 723.5.H8 .C85 2008</b>
                </td>
            </tr>
            </tbody>
        </table>

    </div>

</div>
</body>
<!--Import jQuery before materialize.js-->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="MaterializeCSS/materialize/js/materialize.min.js"></script>
<script>
    $('.button-collapse').sideNav({
            menuWidth: 300, // Default is 300
            edge: 'left', // Choose the horizontal origin
            closeOnClick: false, // Closes side-nav on <a> clicks, useful for Angular/Meteor
            draggable: true // Choose whether you can drag to open on touch screens,
        }
    );

    $(document).ready(function(){
        $('.collapsible').collapsible();
    });
    $(document).ready(function(){
        $('ul.tabs').tabs('select_tab', 'tab_id');
    });
    $(document).ready(function() {
        $('select').material_select();
    });
    $('.datepicker').pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 15, // Creates a dropdown of 15 years to control year,
        today: 'Today',
        clear: 'Clear',
        close: 'Ok',
        closeOnSelect: false // Close upon selecting a date,
    });
</script>
</html>
<?php
/**
 * Created by PhpStorm.
 * User: Joyce Balinquit
 * Date: 15/05/2018
 * Time: 12:34 PM
 */

require "connection.php";
if(isset($_POST['registerAdmin'])){
    $lib_lname = $_POST['lname'];
    $lib_mname = $_POST['mname'];
    $lib_fname = $_POST['fname'];
    $lib_gender = $_POST['gender'];
    $lib_bday = date_create($_POST['birthdate']);
    $lib_email = $_POST['email'];
    $lib_password = $_POST['password'];
    $lib_fpassword = $_POST['fpassword'];
    $lib_position = $_POST['position'];
    $lib_access = $_POST['access'];
    $date = date_format($lib_bday,"Y-m-d");


//Check Librarians
   $check_lib = "SELECT * FROM `librarians` WHERE `email` = '$lib_email'";
   $run_query=mysqli_query($conn,$check_lib);

   if(mysqli_num_rows($run_query)>0) {
       echo "<script>alert('Email Already Taken.');location.href='UserManagement.php';</script>";
   }

 //Check Password and Age
    if(checkPassword($lib_password, $lib_fpassword) == true && checkAge($date) == true){
        echo "<script>alert('Password Mismatch and/or Age Restriction');location.href='UserManagement.php';</script>";
    }
    elseif (checkPassword($lib_password, $lib_fpassword) == false && checkAge($date) == true){
        echo "<script>alert('Age Restriction');location.href='UserManagement.php';</script>";
    }
    elseif (checkPassword($lib_password, $lib_fpassword) == true && checkAge($date) == false){
        echo "<script>alert('Password Mismatch');location.href='UserManagement.php';</script>";
    }
//Insert Librarian
    else

//password hashing
        $hash = password_hash($lib_fpassword, PASSWORD_DEFAULT);

    $con = new mysqli("localhost", "root", "", "nuls2");
    $stmt2  = $con->query("INSERT INTO `librarians` (lname, mname, fname, gender, birthdate, email, password, `position`, date_deleted) VALUES ('$lib_lname', '$lib_mname', '$lib_fname', '$lib_gender', '$date' , '$lib_email', '$hash', '$lib_position', NULL)");
    $id = $con->insert_id;
    echo "<script>alert('id'.$id)</script>";


    if(!empty($_POST['access'])){
        $accesses = $_POST['access'];
        foreach ($accesses as $acc){
            $stmt3 = $conn->query("INSERT INTO `access_levels`(librarian_id,access_level) VALUES('$id','".$acc."')");
        }
    }
    else{
        echo "<script>alert('error'.$conn->error)</script>";
    }

    /**if(!empty($_POST['access'])){
    $accesses = $_POST['access'];
    foreach ($accesses as $acc){
    $stmt3 = $conn->query("INSERT INTO `access_levels`(librarian_id,access_level) VALUES('".$last_id."','".$acc."')");
    }
    }**/
    if($stmt2){
        echo "<script>alert('Librarian Registered Successfully');location.href='UserManagementList.php';</script>";
    }
}

?>
<?php
function checkPassword($pwd1, $pwd2){
    //password checker
    //var msg = (pwd1==pwd2)?"":"Password Mismatch";
    if($pwd1!=$pwd2)
        //echo "<script>alert('Password Mismatch')</script>";
    return true;
}

function checkAge($bday){
    ///age checkerS
    if(!empty($bday)){
        $birth = new DateTime($bday);
        $today = new DateTime('today');
        $age = $birth->diff($today)->y;
        //return $age;
        if($age < 15)
            //echo "<script>alert('Age Restriction. You are only' + $age)</script>";
            return true;
    }


}
?>

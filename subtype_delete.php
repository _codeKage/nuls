<?php
/**
 * Created by PhpStorm.
 * User: Joyce Balinquit
 * Date: 29/05/2018
 * Time: 2:44 PM
 */

require "connection.php";
if(isset($_POST["subtype_id"])){
    if(mysqli_num_rows($conn->query("SELECT * FROM `catalog` WHERE `subtype_id` = '".$_POST["subtype_id"]."' AND `date_deleted` IS NULL")) > 0){
        echo "<script>alert('Cannot Delete Subtype, assigned to one or more Catalog');window.history.back();</script>";
    }
    else {
        $stmt = $conn->query("UPDATE `subtypes` SET date_deleted = CURRENT_DATE WHERE subtype_id = '" . $_POST['subtype_id'] . "'");
        if ($stmt) {
            echo "<script>alert('Subtype Deleted Successfully');location.href='Subtype.php';</script>";
        }
    }

}
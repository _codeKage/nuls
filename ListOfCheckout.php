<?php
include "startup.php";
?>
<!DOCTYPE html>
<html>
<head>
    <!--background-color: #E6BF36;-->

    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="MaterializeCSS/materialize/css/materialize.min.css"  media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="CSS/Style1.css">
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <title>NULRC</title>
</head>
<body>

<div id="container">
    <nav class="nav-background">
        <?php
        include "nav.php";
        ?>

        <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li class="active"><a href="Checkout.php">Checkout</a></li>
            <li class=""><a href="Checkin.php">Checkin</a></li>
        </ul>
        <a href="#" data-activates="slide-out" class="button-collapse hide-on-large-only"><i class="material-icons">menu</i></a>

    </nav>

    <div id="content">
        <div CLASS="row">
            <div class="col s12 m4 l3"></div>

            <div class="col s12 m8 l9">

                <div class="row">
                    <div class="col s12">
                        <div class="input-field col s6">
                            <i class="material-icons prefix">search</i>
                            <input id="icon_prefix" type="text" class="validate">
                            <label for="icon_prefix">Find copy</label>
                        </div>

                        <div class="col s6" style="margin-top: 10px">
                            <a class="waves-effect waves-light btn" style="margin-top: 1%">Go!</a>
                        </div>
                    </div>
                    <div class="col s12">
                        <div class="col s3" style="margin-left: 25px">
                            <input type="checkbox" id="test5" />
                            <label for="test5">Only Search</label>
                        </div>
                        <div class="col s4" style="margin-left: 0px">
                            <select class="browser-default">
                                <option value="1">Patrons Name</option>
                                <option value="2">Book Title</option>
                            </select>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <table class="highlight grey lighten-2">

            <tbody>
            <tr>
                <td>
                    <b>Casil, Dinaline Diaz</b> (BSIT: 2015-102077)<br>
                    <div style="margin-left: 25px">
                    <b>Check Out</b> Library: 1 <br>
                    <b>Overdue</b> Library: 0 <br>
                    <b>Holds Ready</b> 0 <br>
                    </div>
                    <br>
                    <b>Checked Out </b><br>
                    <div style="margin-left: 25px">
                        <b>Java: A beginner's guide</b>
                        (Copy: NULIB000006693)
                    </div>


                </td>
                <td>
                    <a href="Fines.php">Fines</a><br>
                    <b>Library: </b> PHP0.00 <br>
                    <b>Patron: </b> PHP0.00 <br>
                    <b>College / Department: </b> CCS <br><br>

                    <u class="blue-text right-align"><a href="Receipt.html">Receipt</a></u>
                </td>
            </tr>
            </tbody>
        </table>

    </div>

</div>
</body>
<!--Import jQuery before materialize.js-->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="MaterializeCSS/materialize/js/materialize.min.js"></script>
<script>
    $('.button-collapse').sideNav({
            menuWidth: 300, // Default is 300
            edge: 'left', // Choose the horizontal origin
            closeOnClick: false, // Closes side-nav on <a> clicks, useful for Angular/Meteor
            draggable: true // Choose whether you can drag to open on touch screens,
        }
    );

    $(document).ready(function(){
        $('.collapsible').collapsible();
    });
    $(document).ready(function(){
        $('ul.tabs').tabs('select_tab', 'tab_id');
    });
    $(document).ready(function() {
        $('select').material_select();
    });
</script>
</html>
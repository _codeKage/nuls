<?php
/**
 * Created by PhpStorm.
 * User: Joyce Balinquit
 * Date: 31/05/2018
 * Time: 2:10 PM
 */
require "connection.php";
if(isset($_POST["user_id"])){
    if(mysqli_num_rows($conn->query("SELECT * FROM `circulation` WHERE `borrower_id` = '".$_POST["user_id"]."' ")) > 0){
        echo "<script>alert('Cannot Delete User, already had a transaction with the library');window.history.back();</script>";
    }
    else {
        $stmt = $conn->query("UPDATE `users` SET date_deleted = CURRENT_DATE WHERE user_id = '" . $_POST['user_id'] . "'");
        if ($stmt) {
            echo "<script>alert('User Deleted Successfully');location.href='Users.php';</script>";
        }
    }
}
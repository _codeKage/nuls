<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 05/03/2018
 * Time: 8:07 AM
 */
require "connection.php";
session_start();
error_reporting(0);
if(isset($_POST["username"])){
    $email = mysqli_real_escape_string($conn,$_POST['username']);
    $password = mysqli_real_escape_string($conn,$_POST['password']);


    $stmt = $conn->query("SELECT librarian_id, password FROM  `librarians` WHERE email = '".$email."'");
    if($stmt->num_rows > 0){
        $data = $stmt->fetch_array();
        if(password_verify($password, $data['password']))
            $password = $data['password'];

        $stmt1 = $conn->query("SELECT * FROM  `librarians` WHERE email = '".$email."' AND password = '".$password."'");
        $rows = mysqli_num_rows($stmt1);
        if($rows>0){
            while($res = $stmt1->fetch_object()) {
                $_SESSION['username'] = $res->email;
                $_SESSION['position'] = $res->position;
                if($res->position=="Chief Librarian" || $res->position=="Super Admin"){
                    $_SESSION["accesses"] = array();
                    ?>

                    <script>
                        alert('Login Success');
                        location.href="Home.php";
                    </script>
                    <?php
                }
                else{
                    $query = $conn->query("SELECT * FROM `access_levels` WHERE `librarian_id` = '".$res->librarian_id."'");
                    $_SESSION["accesses"] = array();
                    while($row = $query->fetch_object()){
                        array_push($_SESSION["accesses"],$row->access_level);
                    }
                    ?>
                    <script>
                        alert('Login Success');
                        location.href="Home.php";
                    </script>
                    <?php
                }

            }
        }



        else
            echo "<script> alert('Invalid Username and/or Password');location.href=\"Login.html\";</script>";

    }
    else{
        ?>
        <script>
            alert('Invalid Username and/or Password');
            location.href="Login.html";
        </script>

        <?php
    }
}
else{
    session_unset();
    session_destroy();
    header("location:Login.html");
}

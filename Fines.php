<?php
include "startup.php";
?>
<!DOCTYPE html>
<html>
<head>
    <!--background-color: #E6BF36;-->

    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="MaterializeCSS/materialize/css/materialize.min.css"  media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="CSS/Style1.css">
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <title>NULRC</title>
</head>
<body>

<div id="container">
    <nav class="nav-background">
        <?php
        include "nav.php";
        ?>
        <a href="#" data-activates="slide-out" class="button-collapse hide-on-large-only"><i class="material-icons">menu</i></a>

    </nav>

    <!--<div id="content">

        <h5>Casil, Dinaline Diaz (BSIT: 2015-102077)</h5><br>
        <div class="col s10 pull-s1 m6 pull-m4 l5 pull-l4">
        <div class="row">
                <div class="col s6">
                    <div class="col s12">
                        <label>Reason</label>
                        <select class="browser-default">
                            <option value="1">Overdue</option>
                        </select>
                    </div>

                    <div class="input-field col s6">
                        <input id="amount" type="number" class="validate">
                        <label for="amount">Amount</label>
                    </div>

                   

                    <div class="input-field col s12">
                        <i class="material-icons prefix">mode_edit</i>
                        <textarea id="icon_prefix2" class="materialize-textarea"></textarea>
                        <label for="icon_prefix2">Fine Note</label>
                    </div>
                </div>

                <div class="col s12 left-align">
                    <a class="waves-effect waves-light btn" href="ListOfCheckout.php">Save</a>
                    <a class="waves-effect waves-light btn" href="ListOfCheckout.php">Cancel</a>
                </div>

            </div>
        </div>

    </div>-->

    <div id="content">
        <table class="highlight">
            <thead>
                <tr>
                    <th>Circulation ID</th>
                    <th>Book Title</th>
                    <th>Amount</th>
                    <th>Note</th>
                    <th>Reason</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>125</td>
                    <td>Software Engineering</td>
                    <td>200.00</td>
                    <td>
                        <div class="input-field">
                            <input id="note" type="text" class="validate">
                            <label for="note">Note</label>
                        </div>
                    </td>
                    <td>
                        <select class="browser-default">
                            <option value="" disabled selected></option>
                            <option value="overdue">Overdue</option>
                        </select>
                    </td>

                    <td>
                        <a class="waves-effect waves-light btn" href="#" style=" margin-left: 8%">Save</a>
                    </td>
                </tr>

                <tr>
                    <td>126</td>
                    <td>Java Programming</td>
                    <td>200.00</td>
                    <td>
                        <div class="input-field">
                            <input id="note" type="text" class="validate">
                            <label for="note">Note</label>
                        </div>
                    </td>
                    <td>
                        <select class="browser-default">
                            <option value="" disabled selected></option>
                            <option value="overdue">Overdue</option>
                        </select>
                    </td>

                    <td>
                        <a class="waves-effect waves-light btn" href="#" style=" margin-left: 8%">Save</a>
                    </td>
                </tr>
            </tbody>
        </table>

        <div class="right-align">
            <h6><b>Total:</b> P400.00</h6>
            <a class="waves-effect waves-light btn" href="#" style=" margin-left: 8%">Paid</a>
        </div>
    </div>

</div>
</body>
<!--Import jQuery before materialize.js-->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="MaterializeCSS/materialize/js/materialize.min.js"></script>
<script>
    $('.button-collapse').sideNav({
            menuWidth: 300, // Default is 300
            edge: 'left', // Choose the horizontal origin
            closeOnClick: false, // Closes side-nav on <a> clicks, useful for Angular/Meteor
            draggable: true // Choose whether you can drag to open on touch screens,
        }
    );

    $(document).ready(function(){
        $('.collapsible').collapsible();
    });
    $(document).ready(function(){
        $('ul.tabs').tabs('select_tab', 'tab_id');
    });
    $(document).ready(function() {
        $('select').material_select();
    });
</script>
</html>
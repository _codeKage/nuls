<?php
/**
 * Created by PhpStorm.
 * User: Jeremiah
 * Date: 17/07/2018
 * Time: 1:57 PM
 */
require "connection.php";
if(isset($_POST["acquisition_number"])){
    $check = mysqli_num_rows($conn->query("SELECT * FROM `catalog` WHERE `acquisition_number` = '".$_POST["acquisition_number"]."' AND `date_deleted` IS NULL"));
    if($check > 0){
        ?>
        <script>
            alert('Cannot Delete Acquisition, One or more copy was catalogued');
            window.history.back();
        </script>
        <?php
    }else {

        if ($conn->query("UPDATE `acquisition` SET `date_deleted` = '" . date("Y-m-d") . "' WHERE `acquisition_number` = '" . $_POST["acquisition_number"] . "' ")) {
            ?>
            <script>
                alert('Successfully Deleted');
                location.href = "ListOfCopies.php";
            </script>
            <?php
        } else {
            ?>
            <script>
                alert('Deletion Failed');
                location.href = "ListOfCopies.php";
            </script>
            <?php
        }
    }
}else{
    header("location:ListOfCopies.php");
}
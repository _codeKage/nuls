<?php
include "startup.php";
?>
<!DOCTYPE html>
<html>
<head>
    <!--background-color: #E6BF36;-->

    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="MaterializeCSS/materialize/css/materialize.min.css"  media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="CSS/Style1.css">
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <title>NULRC</title>
</head>
<body>

<div id="container">
    <nav class="nav-background">
        <?php
        include "nav.php";
        ?>
        <a href="#" data-activates="slide-out" class="button-collapse hide-on-large-only"><i class="material-icons">menu</i></a>

        <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li class=""><a href="Acquisition_Report.php">Acquisition Report</a></li>
            <li class=""><a href="Catalog_Report.php">Catalog Report</a></li>
            <li class=""><a href="Circulation_Report.php">Circulation Reports</a></li>
            <li class="active"><a href="Inventory.php">Inventory</a></li>
        </ul>
    </nav>

    <div id="content">

        <!--<div class="row">
            <div class="input-field col s4">
                <a href="#" class="prefix"><i class="material-icons">search</i></a>
                <input id="search" placeholder="Search" type="text" class="validate">
            </div>
        </div>-->
        <div class="row s6">
            <form action="inventory_period.php" method="post">
                <input type="submit" value="Start Inventory Period" class="waves-effect waves-light btn active" name="start">
                <input type="submit" value="End Inventory Period" class="waves-effect waves-light btn active" name="end">
            </form>
        </div>
        <div class="row">
            <h6>Date:</h6>
            <div class="input-field col s3">
                <input type="text" class="datepicker" id="to">
                <label class="active center" for="to">To</label>
            </div>
            <div class="input-field col s3">
                <input type="text" class="datepicker" id="from">
                <label class="active center" for="from">From</label>
            </div>
            <div class="col s3">
                <a class="waves-effect waves-light btn active" style="margin-top: 11%" href="Inventory_Print.html">Print</a>
            </div>
            <input type="radio" name="rdb" id="radio1" value="Missing">
            <input type="radio" name="rdb" id="radio1" value="Missing">
        </div>
        <table class="highlight">
            <thead>
            <tr>
                <th>Title</th>
                <th>Author</th>
                <th>Barcode Number</th>
                <th>Remarks</th>
                <th>Status</th>
            </tr>
            </thead>

            <tbody>
            <tr>
                <td>Library acquisition and policies and procedures</td>
                <td>Elizabeth Futas</td>
                <td>NULIB236458871</td>
                <td>Missing Page</td>
                <td>Not Missing
                </td>
            </tr>
            <tr>
                <td>Management of library administration</td>
                <td>Goswami, Inder Mohan</td>
                <td>NULIB687349990</td>
                <td>Covering</td>
                <td>
                    Missing
                </td>
            </tr>
            <tr>
                <td>Provincial library & meseum</td>
                <td>Acierto, Norman E.</td>
                <td>NULIB000006511</td>
                <td>Weed out</td>
                <td>
                    Missing
                </td>
            </tbody>
        </table>

    </div>

</div>
</body>
<!--Import jQuery before materialize.js-->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="MaterializeCSS/materialize/js/materialize.min.js"></script>
<script>
    $('.button-collapse').sideNav({
            menuWidth: 300, // Default is 300
            edge: 'left', // Choose the horizontal origin
            closeOnClick: false, // Closes side-nav on <a> clicks, useful for Angular/Meteor
            draggable: true // Choose whether you can drag to open on touch screens,
        }
    );

    $(document).ready(function(){
        $('.collapsible').collapsible();
    });
    $(document).ready(function(){
        $('ul.tabs').tabs('select_tab', 'tab_id');
    });
    $(document).ready(function() {
        $('select').material_select();
    });
    $('.datepicker').pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 15, // Creates a dropdown of 15 years to control year,
        today: 'Today',
        clear: 'Clear',
        close: 'Ok',
        closeOnSelect: false // Close upon selecting a date,
    });
</script>
</html>
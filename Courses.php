<?php
include "startup.php";
?>
<!DOCTYPE html>
<html>
<head>
    <!--background-color: #E6BF36;-->

    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="MaterializeCSS/materialize/css/materialize.min.css"  media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="CSS/Style1.css">
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <title>NULRC</title>
</head>
<body>

<div id="container">
    <nav class="nav-background">
        <?php
        include "nav.php";
        ?>

        <a href="#" data-activates="slide-out" class="button-collapse hide-on-large-only"><i class="material-icons">menu</i></a>

        <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li><a href="Assets.php">Subject</a></li>

            <li><a href="Supplier.php">Supplier</a></li>
            <li><a href="MaterialType.php">Material Type</a></li>
            <li><a href="Subtype.php">Subtype</a></li>
            <li><a href="Program.php">Programs</a></li>
            <li class="active"><a href="Courses.php">Courses</a> </li>
            <li><a href="Users.php">Users</a></li>
        </ul>
    </nav>

    <div id="content">

        <div class="fixed-action-btn horizontal click-to-toggle">
            <a class="btn-floating btn-large red" href="Add_Course.php">
                <i class="material-icons">add</i>
            </a>
        </div>

        <table class="highlight">
            <thead>
            <tr>
                <th>Course</th>
                <th>College</th>
                <th>Manage</th>
            </tr>
            </thead>
            <tbody>
            <?php
            require "connection.php";
            $stmt  = $conn->query("SELECT * FROM `courses` INNER JOIN `programs` WHERE `courses`.`program_id` = `programs`.`program_id` ");
            $ctr = mysqli_num_rows($stmt);
            $page = $ctr/10;
            $page = ceil($page);
            for($b=1; $b<=$page; $b++){
                ?> <a href="Courses.php?page=<?php echo $b;?>"><?php echo $b.' '?></a>  <?php
            }
            $a = 1;
            if(isset($_GET["page"])) {
                $a = $_GET["page"];
            };
            if($a =="" || $a == "1") {
                $page1=0;
            }
            else {
                $page1=($a*10)-10;
            }
            $stmt2  = $conn->query("SELECT * FROM `courses` INNER JOIN `programs` WHERE `courses`.`program_id` = `programs`.`program_id` LIMIT $page1, 10 ");
            if(mysqli_num_rows($stmt2) > 0){
                while ($row = mysqli_fetch_array($stmt2)){
                    echo "<tr><td>".$row["course"]." </td> <td>". $row["program"]."</td> <td style='display: flex'><form action='Update_Course.php' method='get'><input type='hidden' value='".$row["course_id"]. "' name='course_id'>
                        <input class='btn-floating material-icons' type='submit' value='edit' style='border: 0px;color: #e4ffda;font-size: x-large;margin-right:25px'></form>
                        <form action='course_delete.php' method='post'><input class='btn-floating material-icons' type='submit' value='delete' style='border: 0px;color: white;font-size: x-large;'><input type='hidden' name='course_id' value='" .$row["course_id"]."'></form></td></tr>";
                }
            }
            ?>
            </tbody>
        </table>
    </div>

</div>
</body>
<!--Import jQuery before materialize.js-->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="MaterializeCSS/materialize/js/materialize.min.js"></script>
<script>
    $('.button-collapse').sideNav({
            menuWidth: 300, // Default is 300
            edge: 'left', // Choose the horizontal origin
            closeOnClick: false, // Closes side-nav on <a> clicks, useful for Angular/Meteor
            draggable: true // Choose whether you can drag to open on touch screens,
        }
    );

    $(document).ready(function(){
        $('.collapsible').collapsible();
    });
    $(document).ready(function(){
        $('ul.tabs').tabs('select_tab', 'tab_id');
    });
    $(document).ready(function() {
        $('select').material_select();
    });
    $('.datepicker').pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 15, // Creates a dropdown of 15 years to control year,
        today: 'Today',
        clear: 'Clear',
        close: 'Ok',
        closeOnSelect: false // Close upon selecting a date,
    });
</script>
</html>
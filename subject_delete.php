<?php
/**
 * Created by PhpStorm.
 * User: Joyce Balinquit
 * Date: 28/05/2018
 * Time: 3:14 PM
 */
require "connection.php";
if(isset($_POST["subject_id"])){
    if(mysqli_num_rows($conn->query("SELECT * FROM `acquisition` WHERE `subject_id` = '".$_POST["subject_id"]."' AND `date_deleted` IS NULL")) > 0){
        echo "<script>alert('Cannot Delete Subject, it is assigned to one or more acquisition');window.history.back();</script>";
    }
    else {
        $stmt = $conn->query("UPDATE `subjects` SET date_deleted = CURRENT_DATE WHERE subject_id = '" . $_POST['subject_id'] . "'");
        if ($stmt) {
            echo "<script>alert('Subject Deleted Successfully');location.href='Assets.php';</script>";
        }
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: Jeremiah
 * Date: 22/07/2018
 * Time: 1:08 AM
 */
require "connection.php";
if(!isset($_POST["catalog_id"])){
    header("location:Catalog.php");
}
else{
    $check = mysqli_num_rows($conn->query("SELECT * FROM `catalog` INNER JOIN `circulation` WHERE `catalog`.`catalog_id` = '".$_POST["catalog_id"]."' AND `circulation`.`barcode` = `catalog`.`barcode` "));
    if($check > 0){
        echo "<script>
                alert('Cannot Delete Catalog, Being used in Circulation');
                window.history.back();
            </script>";
    }
    else {
        if ($stmt = $conn->query("UPDATE `catalog` SET `date_deleted` = '" . date("Y-m-d") . "' WHERE `catalog_id` ='" . $_POST["catalog_id"] . "'")) {
            echo "<script>
                alert('Successfully Deleted');
                window.history.back();
            </script>";
        } else {
            echo "<script>
                alert('Deletion Failed');
                window.history.back();
            </script>";

        }
    }
}
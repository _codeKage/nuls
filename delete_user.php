<?php
/**
 * Created by PhpStorm.
 * User: Joyce Balinquit
 * Date: 16/05/2018
 * Time: 8:47 PM
 */
require "connection.php";
if(isset($_POST["librarian_id"])){
    if(mysqli_num_rows($conn->query("SELECT * FROM `circulation` WHERE `lender_id` = '".$_POST["librarian_id"]."' OR `receiver_id` = '".$_POST["librarian_id"]."'")) > 0){
        echo "<script>alert('Cannot Delete User, Already had a transaction with the library');window.history.back();</script>";
    }
    else {
        $stmt = $conn->query("UPDATE `librarians` SET date_deleted = CURRENT_DATE WHERE librarian_id = '" . $_POST['librarian_id'] . "'");
        if ($stmt) {
            echo "<script>alert('Librarian Deleted Successfully');location.href='UserManagementList.php';</script>";
        }
    }

}
else{
    header("location:UserManagementList.php");
}